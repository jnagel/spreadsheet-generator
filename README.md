# Building the server

Go to the project directory  and run the following command to extract  executable file of the server.
```$ go build -o excel ```

Deploy
```$ git push heroku master ```

Filling in important values in the source code.
* Since it was noted in the spec that the compressed files will be sent to a single person
 you should provide the email address in `main.go` on line `22`.
* Also you should provide an API link which is for fetching franchise IDs in `main.go` on line `25`
* Mailgun domain address for the mailgun API in `main.go` on line `61`
* Mailgun API key in `main.go` on line `61`
* Enter API url for fetching franchise data in `api.go` on line `36`
* Enter message sender information in `email.go` on line `10`
