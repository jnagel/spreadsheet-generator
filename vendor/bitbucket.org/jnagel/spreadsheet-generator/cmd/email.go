package cmd

import (
	"github.com/mailgun/mailgun-go"
)

// SendAttachment sends zipped file to given email address
func SendAttachment(domain, apiKey, emailAddr, zipName string) (string, error) {
	mg := mailgun.NewMailgun(domain, apiKey, "")
	m := mg.NewMessage("My User <EMAIL_ADDR@MYDOMAIN.COM>",
		"Generated your report",
		"We generated a report for you.",
		emailAddr)
	m.AddAttachment("./" + zipName)
	_, id, err := mg.Send(m)
	if err != nil {
		return "", err
	}
	return id, nil
}
