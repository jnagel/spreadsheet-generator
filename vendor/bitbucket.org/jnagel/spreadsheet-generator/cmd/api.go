package cmd

import (
	"encoding/json"
	"net/http"
)

type Franchise struct {
	ID int `json:"franchise_id"`
}

func GetFranchises(endpoint string) ([]Franchise, error) {
	res, err := http.Get(endpoint)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	var franchises []Franchise
	if err := json.NewDecoder(res.Body).Decode(&franchises); err != nil {
		return nil, err
	}

	return franchises, nil
}

type FranchiseData struct {
	CustomerID         int    `json:"id"`
	CurrentStatus      string `json:"Customer_Status"`
	CurrentStatusNotes string `json:"Custom_Status_Notes"`
	ReferredDate       string `json:"Created_Time"`
	ReferrerName       string `json:"Referral_Partner_Name"`
	ProductsOfInterest string `json:"Current Service Types"`
}

var franchiseDataURL = "https://crmsandbox.zoho.com/crm/v2/functions/get_referred_clients_for_franchise/actions/execute?auth_type=apikey&zapikey=1003.82c5b96a4725e4c44326bdf3ec827c2a.dc07001210f4a711bd6b9f34ec12d80b"

func FetchFranchiseData(franchiseID int) ([]FranchiseData, error) {
	res, err := http.Get(franchiseDataURL)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	var franchiseData []FranchiseData
	if err := json.NewDecoder(res.Body).Decode(&franchiseData); err != nil {
		return nil, err
	}

	return franchiseData, nil
}
