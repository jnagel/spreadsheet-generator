package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"bitbucket.org/jnagel/spreadsheet-generator/cmd"
	//_ "github.com/heroku/x/hmetrics/onload"
)

func main() {
	http.HandleFunc("/", sayHello)
	http.HandleFunc("/generate_reports", generateReports)
	port := os.Getenv("PORT")
	fmt.Println("PORT!!!", port)
	if port == "" {
		//log.Fatal("$PORT must be set")
		log.Println("PORT is not defined")
		port = "8000"
	}
	if err := http.ListenAndServe(":"+port, nil); err != nil {
		panic(err)
	}
}

func sayHello(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("hello world. server is running!"))
}

var emailAddr = "jeremy.nagel+ifis@nuanced.it"

func generateReports(w http.ResponseWriter, r *http.Request) {
	log.Println("Fetching franchises sdf")
	franchises, err := cmd.GetFranchises("https://crm.zoho.com/crm/v2/functions/get_franchises/actions/execute?auth_type=apikey&zapikey=1003.e189e689c8be9c90f6bbd3d7f46c6b06.46cf62769074d828260d1f41881e6984")
	if err != nil {
		w.Write([]byte(fmt.Sprintf("error while fetching franchises: %v", err)))
		return
	}

	// creating a dir where reports will go into
	dirName := cmd.CreateDir()

	fileNames := []string{}
	for _, franchise := range franchises {
		// fetching a franchise data
		franchiseData, err := cmd.FetchFranchiseData(franchise.ID)
		if err != nil {
			w.Write([]byte(fmt.Sprintf("error while fetching franchise data: %v", err)))
			return
		}

		// generating report
		fileName, err := cmd.GenerateReport(franchiseData, franchise.ID, dirName)
		if err != nil {
			w.Write([]byte(fmt.Sprintf("error while generating report: %v", err)))
			return
		}
		fileName = dirName + "/" + fileName // registering file in the newly created dir
		fileNames = append(fileNames, fileName)
	}

	// zipping files
	zipName := dirName + ".zip"
	if err := cmd.ZipFiles(zipName, fileNames); err != nil {
		w.Write([]byte(fmt.Sprintf("error while zipping files: %v", err)))
		return
	}

	// Enter your domain, mailgun api key, email address to send to, and zipname
	_, err = cmd.SendAttachment("sandbox8df3507c0f80457db2df3e1edfa0f3d3.mailgun.org", "a2de9d5599d8337c81c58da1b0f729e2-47317c98-a6f60abd", emailAddr, zipName)
	if err != nil {
		w.Write([]byte(fmt.Sprintf("error while sending zipped file: %v", err)))
		return
	}

	w.Write([]byte(fmt.Sprintf("Successfully sent reports to %s", emailAddr)))
}
