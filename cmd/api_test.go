package cmd

import (
	"testing"
)

func TestSingleFranchise(t *testing.T) {
	data := []FranchiseData{
		FranchiseData{
			CustomerID:         1,
			CurrentStatus:      "Test Status 1",
			CurrentStatusNotes: "Note 1",
			ReferredDate:       "12-01-2018",
			ReferrerName:       "Referrer 1",
			ProductsOfInterest: "Product 1",
		},
		FranchiseData{
			CustomerID:         2,
			CurrentStatus:      "Test Status 2",
			CurrentStatusNotes: "Note 2",
			ReferredDate:       "12-02-2018",
			ReferrerName:       "Referrer 2",
			ProductsOfInterest: "Product 2",
		},
	}
	dirname := CreateDir()
	if _, err := GenerateReport(data, 1, dirname); err != nil {
		t.Fatal(err)
	}
}

func TestMultipleFranchises(t *testing.T) {
	franchises := []Franchise{
		Franchise{
			ID: 1,
		},
		Franchise{
			ID: 2,
		},
	}

	franchiseOneData := []FranchiseData{
		FranchiseData{
			CustomerID:         1,
			CurrentStatus:      "Test Status 1",
			CurrentStatusNotes: "Note 1",
			ReferredDate:       "12-01-2018",
			ReferrerName:       "Referrer 1",
			ProductsOfInterest: "Product 1",
		},
		FranchiseData{
			CustomerID:         2,
			CurrentStatus:      "Test Status 2",
			CurrentStatusNotes: "Note 2",
			ReferredDate:       "12-02-2018",
			ReferrerName:       "Referrer 2",
			ProductsOfInterest: "Product 2",
		},
	}

	franchiseTwoData := []FranchiseData{
		FranchiseData{
			CustomerID:         3,
			CurrentStatus:      "Test Status 3",
			CurrentStatusNotes: "Note 3",
			ReferredDate:       "12-01-2018",
			ReferrerName:       "Referrer 3",
			ProductsOfInterest: "Product 3",
		},
		FranchiseData{
			CustomerID:         4,
			CurrentStatus:      "Test Status 4",
			CurrentStatusNotes: "Note 4",
			ReferredDate:       "12-02-2018",
			ReferrerName:       "Referrer 4",
			ProductsOfInterest: "Product 4",
		},
	}

	dirname := CreateDir()
	fileNameOne, _ := GenerateReport(franchiseOneData, franchises[0].ID, dirname)
	fileNameTwo, _ := GenerateReport(franchiseTwoData, franchises[1].ID, dirname)
	fileNames := []string{fileNameOne, fileNameTwo}
	zipName := dirname + ".zip"
	if err := ZipFiles(zipName, fileNames); err != nil {
		t.Fatalf("can't zip files: %v", err)
		return
	}
}
