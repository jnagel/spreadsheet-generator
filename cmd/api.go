package cmd

import (
	"encoding/json"
	"net/http"
)

type Franchise struct {
	ID int `json:"franchise_id"`
}

type FranchiseResponse struct {
	Code    string `json:"code"`
	Details struct {
		Output     string `json:"output"`
		OutputType string `json:"output_type"`
		ID         string `json:"id"`
	} `json:"details"`
	Message string `json:"message"`
}

func GetFranchises(endpoint string) ([]Franchise, error) {
	log.Println("here goes fsdfnothing")
	res, err := http.Get(endpoint)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	var franchiseResponse FranchiseResponse
	log.Println("here goes nothing")
	log.Println(res.Body)

	if err := json.NewDecoder(res.Body).Decode(&franchiseResponse); err != nil {
		log.Println("Uh oh")
		log.Println(err)
		return nil, err
	}

	log.Println("Decoded franchise response")

	franchises := franchiseResponse["details"]["franchises"]

	return franchises, nil
}

type FranchiseData struct {
	CustomerID         int    `json:"id"`
	CurrentStatus      string `json:"Customer_Status"`
	CurrentStatusNotes string `json:"Custom_Status_Notes"`
	ReferredDate       string `json:"Created_Time"`
	ReferrerName       string `json:"Referral_Partner_Name"`
	ProductsOfInterest string `json:"Current Service Types"`
}

var franchiseDataURL = "https://crm.zoho.com/crm/v2/functions/get_referred_clients_for_franchise/actions/execute?auth_type=apikey&zapikey=1003.e189e689c8be9c90f6bbd3d7f46c6b06.46cf62769074d828260d1f41881e6984"

func FetchFranchiseData(franchiseID int) ([]FranchiseData, error) {
	res, err := http.Get(franchiseDataURL)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	var franchiseData []FranchiseData
	if err := json.NewDecoder(res.Body).Decode(&franchiseData); err != nil {
		return nil, err
	}

	return franchiseData, nil
}
